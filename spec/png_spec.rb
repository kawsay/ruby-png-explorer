$LOAD_PATH.unshift(File.dirname(__FILE__))
require 'rspec'
require 'zlib'
require 'png/png.rb'
require 'png/chunk.rb'

RSpec.describe Png do

  context 'when valid png file' do
    let(:png) { Png.new('png.png') }

    describe '#class' do
      it 'creates a Png object' do		
        expect(png.class).to eql(Png)
      end
    end

    describe '#png?' do
      it 'validates correct signatures' do
        expect(png.png?).to eql(true)
      end
    end

    describe '#chunk!' do
      it 'sets chunks' do
        expect(!!png.chunks).to eq(true)
      end
    end

    describe '#chunk!' do
      it 'creates at least 2 chunks' do
        expect(png.chunks.size).to be > 2
      end
    end

    describe '#chunk!' do
      it 'catches IHDR as 1st chunk' do
        expect(png.chunks[0][0].type).to eq('IHDR')
      end
    end

    describe '#chunks!' do
      it 'catches IEND as last chunk' do
        expect(png.chunks[-1][0].type).to eq('IEND')
      end
    end
  end

  context 'when invalid png file' do	
    let(:jpg) { Png.new('jpg.jpg') }

    describe '#png?' do
      it 'detects incorrect signatures' do
        expect(jpg.png?).not_to eq(true)
      end
    end

    describe '.chunks' do
      it 'sets @chunks to nil' do
        expect(jpg.chunks).to eq(nil)
      end
    end
  end
end
