class Png
  attr_reader :filename, :content, :chunks
  SIGNATURE	= "\x89PNG\r\n\x1A\n" 

  def initialize(filename)
    @content   = File.open(filename)
    @signature = @content.read(8).bytes
    self.png? ? @chunks = chunk! : nil
  end

  def png?
    @signature == SIGNATURE.bytes
  end

  def chunk!
    chunks = [[Chunk.new(@content)]]
    until chunks[-1][0].type == 'IEND'
        chunks << [Chunk.new(@content)] 
    end
    chunks
  end
end

