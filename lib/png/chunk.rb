class Chunk
  attr_reader :content, :length, :type, :data, :crc

  def initialize(content)
    @content = content
    @length  = get_int(4)
    @type    = @content.read(4)
    @data    = @content.read(@length)
    @crc     = @content.read(4)
  end

  def get_int(bytes)
    get_hex(bytes).to_i(16)
  end

  def get_hex(bytes)
    @content.read(bytes).bytes.pack('c*').unpack('H*').first
  end

  def get_readable_hex(bytes)
    @content.read(bytes).bytes.pack('c*').unpack('H*').first.gsub /(.{2})/, '\1 '
  end

  def valid_crc?
    Zlib::crc32(@type + @data) == crc_to_int
  end

  def crc_to_int
    @crc.bytes.pack('c*').unpack('H*').first.to_i(16)
  end
end

