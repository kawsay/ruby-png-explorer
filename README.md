# PNG file decoder
----

## Disclaimer
I made it for learning purposes, I wouldn't recommend anyone to use it (but if you do, I'd be glad someone uses it! )

## What's this project ?
It's a Ruby CLI tool that allows you to get more informations about a PNG file.

## How to use it ?
Load a file:
``` ruby
    my_file = Png.new('png.png')
    => #<Png:0x00004242424242> 
```

It'll automaticaly be *chunkated*
``` ruby    
    my_file.chunks.size
    => 15
```

You can access individual chunks:
``` ruby    
    my_file.chunks.first
    => [#<Chunk:0x00005560d26837a0
         @content=#<File:../png.png>,
         @crc="\xE4\xF7\xE2?",
         @data="\x00\x00\x02\xA6\x00\x00\x01\x84\b\x06\x00\x00\x00",
         @length=13,
         @type="IHDR">]
```

Chunks are grouped in an Array, each chunk has its own array:
``` ruby    
    [
      [Chunk1, @type, @length, @data, @crc, @content],
      [Chunk2, @type, @length, @data, @crc, @content],
      ...
    ]
```

## Why use it ?
¯\\_(ツ)_/¯

<hr>

### Want to learn more about PNGs ?
Here are some useful resources that might interest you:
  
  * [The PNG Specification](https://www.w3.org/TR/PNG/) (Bible)
  * [Dalke Scientific](http://www.dalkescientific.com/writings/diary/archive/2014/07/10/png_checksum.html), great reading about CRC
  * [Zenhack CTF about PNG](https://zenhack.it/challenges/2017/05/30/pngcrc/), quick summary + a great challenge (guided)
  